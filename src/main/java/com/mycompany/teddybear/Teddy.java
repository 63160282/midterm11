/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teddybear;

/**
 *
 * @author User
 */
public class Teddy {
    public String name;
    public String color;
    
    Teddy(String name,String color){
        this.name = name;
        this.color = color;
    }
    public void speak(){
        System.out.println("Teddy Speak");
        System.out.println("my name is "+this.name+" I'm color "+this.color);
    }
    public void sing1(){
        System.out.println("Teddy sing a song");
        System.out.println("Twinkle, twinkle, little star\n" 
                +"How I wonder what you are\n" 
                +"Up above the world so high\n"
                +"Like a diamond in the sky\n" 
                +"Twinkle, twinkle, little star\n" 
                +"How I wonder what you are");
    }
    public void sing2(){
        System.out.println("Teddy sing a song");
        System.out.println("Snowflake, snowflake, little snowflake\n" 
                +"Little snowflake\n" 
                +"Falling from the sky\n" 
                +"Snowflake, snowflake, little snowflake\n" +"Falling, falling, falling, falling, falling\n" 
                +"Falling, falling, falling, falling\" "
                +"Falling on my head");
    
    }
    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }
    
}
